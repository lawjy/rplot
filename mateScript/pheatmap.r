#!/usr/bin/Rscript
# -*- coding: utf-8 -*-

suppressPackageStartupMessages(library(argparse))
docstring = "Description::\\n This Script is designes to plot Heatmap. \\n Author: Rojyang."
myparser <- ArgumentParser(description=docstring, formatter_class="argparse.RawTextHelpFormatter")
myparser$add_argument("r.abundance_file", nargs=1, help="")
myparser$add_argument("group", nargs=1, help="")
myparser$add_argument("-f","--prefix", nargs=1,  default="defalt", help=" Prefix of the figures")
myparser$add_argument('-o',"--outdir", default=getwd(), help="outdir")
myparser$add_argument('-s',"--topN", default="50", help="top number ,default 50")
args <- myparser$parse_args()

inputfile = args$r.abundance_file
groupfile = args$group
svgprefix = args$prefix
output = args$outdir
svgfile = paste(output, (paste(svgprefix, "svg", sep = ".")), sep = "/")
topN = as.numeric(args$topN)


library(vegan)
library(ggplot2)
library(pheatmap)

if(FALSE){
inputfile = c("data/total.S.annotation.relative_abundance")
groupfile = c("data/treatment.txt")
topN = 50
}

data = read.table(inputfile, head=T, row.names = 1, sep="\t", quote="")
group=read.table(groupfile,head=T, sep = "\t")

data = data [as.character(group$Sample)]
## 取前50的物种
#apply(data, 1, sum)
data2 = data[order(apply(data, 1, sum), decreasing=TRUE)[2:topN],]
topNData=as.matrix(data2)

## log均一化
topNDataLog=log2(topNData+1)
annotation_col = data.frame(group=group$treatment)
rownames(annotation_col) = colnames(topNDataLog)

### 绘图
svg(svgfile, width = 10, height = 8)
pheatmap(topNDataLog,  annotation_col = annotation_col,scale = 'row')
#pheatmap(topNDataLog1,scale = 'row')
dev.off()

