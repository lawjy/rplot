OTU ID	BA2	BA3	BA1	BN3	BN2	BN1
1,1,1-Trichloro-2,2-bis(4-chlorophenyl)ethane (DDT) degradation	12	11	13	8	8	8
ABC transporters	42341	63446	45846	53537	41433	55267
Adipocytokine signaling pathway	1033	1256	1156	1076	975	1054
African trypanosomiasis	97	116	117	55	37	61
Alanine, aspartate and glutamate metabolism	15249	19161	16095	17122	15149	17420
alpha-Linolenic acid metabolism	212	184	152	96	81	131
Alzheimer's disease	940	1103	985	866	765	908
Amino acid metabolism	3029	3779	3170	3546	2982	3567
Amino acid related enzymes	21155	27324	22616	24306	20876	24699
Amino sugar and nucleotide sugar metabolism	19066	24969	20423	22503	19446	22986
Aminoacyl-tRNA biosynthesis	16956	22819	18367	19996	16769	20261
Aminobenzoate degradation	2911	3099	2785	2250	1815	2410
Amoebiasis	158	244	232	220	159	221
Amyotrophic lateral sclerosis (ALS)	583	579	580	529	497	561
Antigen processing and presentation	596	763	655	722	624	712
Apoptosis	63	25	52	23	26	28
Arachidonic acid metabolism	375	392	329	212	163	248
Arginine and proline metabolism	18138	23568	19380	21013	17912	21173
Ascorbate and aldarate metabolism	1895	2424	1910	2070	1767	2113
Atrazine degradation	487	556	419	453	323	485
Bacterial chemotaxis	5924	9485	6676	8335	5838	8796
Bacterial invasion of epithelial cells	17	6	8	4	4	3
Bacterial motility proteins	14188	20574	15422	18059	12117	18908
Bacterial secretion system	9633	11604	10090	9702	8247	9628
Bacterial toxins	1622	2274	1837	1987	1701	2015
Basal transcription factors	14	37	12	4	3	18
Base excision repair	6296	8345	6671	7072	6021	7288
Benzoate degradation	4434	5378	4122	3993	3093	4204
beta-Alanine metabolism	3960	3810	3639	3007	2828	3275
beta-Lactam resistance	393	329	399	298	326	310
Betalain biosynthesis	2	9	4	2	1	1
Bile secretion	4	2	7	2	1	2
Biosynthesis and biodegradation of secondary metabolites	947	1184	975	1013	830	994
Biosynthesis of ansamycins	1306	2099	1453	1910	1510	1880
Biosynthesis of siderophore group nonribosomal peptides	366	354	259	220	170	242
Biosynthesis of type II polyketide products	6	14	4	0	2	3
Biosynthesis of unsaturated fatty acids	2195	2384	1904	1750	1403	1838
Biosynthesis of vancomycin group antibiotics	945	1082	1053	1057	969	1055
Biotin metabolism	2418	2531	2439	2282	2158	2294
Bisphenol degradation	1342	1654	1293	1409	1169	1433
Bladder cancer	19	25	13	3	4	13
Butanoate metabolism	10867	13609	11163	11446	9277	11747
Butirosin and neomycin biosynthesis	1047	1434	1161	1347	1163	1305
C5-Branched dibasic acid metabolism	4394	5986	4786	5368	4482	5389
Caffeine metabolism	14	6	10	0	1	2
Calcium signaling pathway	1	0	0	2	0	0
Caprolactam degradation	1217	934	947	469	347	577
Carbohydrate digestion and absorption	117	290	136	208	142	151
Carbohydrate metabolism	1762	2951	1900	2521	2002	2548
Carbon fixation in photosynthetic organisms	8632	11532	9272	10388	8935	10365
Carbon fixation pathways in prokaryotes	16077	19253	17201	17129	14792	17108
Cardiac muscle contraction	152	120	153	51	69	78
Carotenoid biosynthesis	119	146	147	97	63	63
Cell cycle - Caulobacter	7370	9250	7946	8248	7239	8457
Cell division	1239	1239	1272	1209	1169	1217
Cell motility and secretion	3043	3154	3150	2801	2430	2726
Cellular antigens	787	697	870	675	683	692
Chagas disease (American trypanosomiasis)	52	60	48	9	8	25
Chaperones and folding catalysts	15273	18042	15828	16179	14382	16440
Chloroalkane and chloroalkene degradation	2798	3875	2808	3106	2464	3127
Chlorocyclohexane and chlorobenzene degradation	412	376	367	207	158	265
Chromosome	22165	28655	23461	25504	21730	25881
Circadian rhythm - plant	26	19	17	1	2	7
Citrate cycle (TCA cycle)	10539	12242	11365	10712	9487	10548
Colorectal cancer	27	8	29	1	9	5
Cyanoamino acid metabolism	4106	5158	4293	4823	4344	5016
Cysteine and methionine metabolism	12599	16662	13388	14698	12508	14900
Cytochrome P450	1	1	1	0	0	0
Cytoskeleton proteins	4713	6909	5167	6153	4965	6153
D-Alanine metabolism	1622	2131	1751	1897	1564	1931
D-Arginine and D-ornithine metabolism	79	104	95	107	77	114
D-Glutamine and D-glutamate metabolism	2328	2814	2443	2541	2235	2559
Dioxin degradation	651	1203	714	979	690	911
DNA repair and recombination proteins	40304	52940	42862	46354	39734	47245
DNA replication	9751	12661	10450	11196	9696	11446
DNA replication proteins	17504	22777	18721	20464	17749	20843
Drug metabolism - cytochrome P450	980	822	800	469	359	497
Drug metabolism - other enzymes	4494	5963	4748	5485	4731	5701
Electron transfer carriers	724	1205	1326	1111	694	883
Endocytosis	9	0	1	0	0	0
Energy metabolism	14403	17895	15817	16512	14132	16326
Epithelial cell signaling in Helicobacter pylori infection	1205	1569	1252	1486	1275	1494
Ether lipid metabolism	59	66	47	25	29	35
Ethylbenzene degradation	916	1104	876	912	746	910
Fat digestion and absorption	3	2	1	0	0	0
Fatty acid biosynthesis	6668	9084	7138	7662	6392	7673
Fatty acid elongation in mitochondria	11	1	6	0	1	0
Fatty acid metabolism	4947	5351	4486	3733	3057	4052
Fc gamma R-mediated phagocytosis	12	2	2	0	0	0
Flagellar assembly	6165	9151	6936	8559	5833	9067
Flavone and flavonol biosynthesis	102	156	116	174	135	194
Flavonoid biosynthesis	118	195	142	174	122	152
Fluorobenzoate degradation	264	150	157	44	36	69
Folate biosynthesis	6029	6579	6042	5682	5334	5895
Fructose and mannose metabolism	11542	16112	11985	14116	11946	14167
Function unknown	20248	24792	21003	21025	17366	21257
Galactose metabolism	9890	12856	10407	12218	10861	12390
General function prediction only	50787	65837	53343	57110	48446	58362
Geraniol degradation	1940	1441	1435	719	640	901
Germination	375	790	388	640	475	685
Glutamatergic synapse	1460	1996	1572	1710	1477	1750
Glutathione metabolism	3416	3495	3195	2653	2356	2918
Glycan biosynthesis and metabolism	630	658	660	555	463	509
Glycerolipid metabolism	5803	8238	6095	7341	6044	7432
Glycerophospholipid metabolism	7772	9938	8003	8561	7459	8751
Glycine, serine and threonine metabolism	12499	15557	13240	13693	11964	13923
Glycolysis / Gluconeogenesis	15316	21053	16189	18031	15150	18125
Glycosaminoglycan degradation	1669	1277	1730	1519	1720	1526
Glycosphingolipid biosynthesis - ganglio series	1250	907	1307	1120	1292	1106
Glycosphingolipid biosynthesis - globo series	2232	2229	2368	2526	2487	2531
Glycosphingolipid biosynthesis - lacto and neolacto series	2	2	2	2	3	2
Glycosyltransferases	4562	5371	4573	4564	4026	4695
Glyoxylate and dicarboxylate metabolism	8040	9873	8326	8411	7158	8685
GnRH signaling pathway	9	0	1	0	0	0
Hematopoietic cell lineage	1	1	1	0	0	0
Histidine metabolism	9448	11834	9977	10512	9127	10636
Homologous recombination	13653	17779	14630	15819	13580	15979
Huntington's disease	772	696	743	561	552	614
Hypertrophic cardiomyopathy (HCM)	1	1	5	0	0	1
Indole alkaloid biosynthesis	1	6	4	2	1	1
Influenza A	27	8	29	1	9	5
Inorganic ion transport and metabolism	3327	3704	3323	3094	2714	3165
Inositol phosphate metabolism	1853	2321	1881	1906	1614	1993
Insulin signaling pathway	995	1543	1033	1303	1048	1259
Ion channels	241	227	214	163	140	181
Isoflavonoid biosynthesis	2	2	3	0	0	1
Isoquinoline alkaloid biosynthesis	691	716	696	663	613	675
Limonene and pinene degradation	2694	2662	2335	1871	1560	2030
Linoleic acid metabolism	997	1249	953	1089	922	1095
Lipid biosynthesis proteins	8915	11463	9402	9653	8191	9784
Lipid metabolism	1771	2566	1857	2182	1696	2158
Lipoic acid metabolism	846	816	792	751	750	755
Lipopolysaccharide biosynthesis	4694	3633	4603	3587	3777	3522
Lipopolysaccharide biosynthesis proteins	6259	5424	6165	5144	5172	5123
Lysine biosynthesis	11332	15239	12301	13777	11882	13856
Lysine degradation	3235	3213	2788	2142	1827	2450
Lysosome	2499	2242	2683	2660	2728	2699
MAPK signaling pathway - yeast	835	876	893	901	852	923
Meiosis - yeast	73	68	97	32	19	40
Melanogenesis	1	3	0	0	0	0
Membrane and intracellular structural molecules	9201	8520	9325	7835	7729	7852
Metabolism of cofactors and vitamins	1608	1987	1672	1496	1193	1478
Metabolism of xenobiotics by cytochrome P450	931	791	769	464	350	487
Methane metabolism	17832	24498	19662	22390	18877	22353
Mineral absorption	111	142	113	87	47	87
Mismatch repair	11649	15432	12425	13684	11722	13961
mRNA surveillance pathway	0	0	2	0	0	0
N-Glycan biosynthesis	383	389	429	393	372	393
Naphthalene degradation	2630	3194	2445	2552	2030	2506
Nicotinate and nicotinamide metabolism	6268	7796	6512	6824	5976	6835
Nitrogen metabolism	10198	12922	11019	11157	9585	11319
Nitrotoluene degradation	893	1380	1153	1201	886	1162
NOD-like receptor signaling pathway	601	768	660	726	629	716
Non-homologous end-joining	210	349	300	242	151	163
Novobiocin biosynthesis	1896	2330	2036	2119	1843	2139
Nucleotide excision repair	5547	7428	5978	6479	5556	6610
Nucleotide metabolism	771	964	838	897	727	869
One carbon pool by folate	9240	11465	9756	10474	9241	10709
Other glycan degradation	5177	5369	5602	6091	5969	6179
Other ion-coupled transporters	17996	22696	18229	19208	16395	19613
Other transporters	3478	4835	3735	4151	3390	4108
Others	13128	17806	13539	15098	12491	15666
Oxidative phosphorylation	18438	21069	19165	18635	17088	19460
p53 signaling pathway	29	13	35	1	9	7
Pantothenate and CoA biosynthesis	8902	11204	9292	10022	8736	10245
Parkinson's disease	185	135	193	55	78	85
Pathways in cancer	794	933	826	795	707	819
Penicillin and cephalosporin biosynthesis	433	345	446	306	304	310
Pentose and glucuronate interconversions	6802	9729	7346	8942	7562	8947
Pentose phosphate pathway	11140	16223	11979	14228	11785	14220
Peptidases	26008	33648	27831	30528	26337	31004
Peptidoglycan biosynthesis	11265	15135	12157	13285	11218	13432
Peroxisome	3245	3534	3309	3077	2867	3203
Pertussis	546	387	433	290	310	274
Phenylalanine metabolism	3077	3780	3317	3062	2519	3066
Phenylalanine, tyrosine and tryptophan biosynthesis	11871	15215	12581	13699	11866	13692
Phenylpropanoid biosynthesis	2123	2639	2183	2592	2428	2755
Phosphatidylinositol signaling system	1374	1632	1453	1404	1233	1409
Phosphonate and phosphinate metabolism	863	1217	939	1080	868	1034
Phosphotransferase system (PTS)	4564	6205	4047	4739	3902	5212
Photosynthesis	5277	6168	5316	5493	4925	5764
Photosynthesis - antenna proteins	17	0	0	34	0	0
Photosynthesis proteins	5440	6291	5464	5639	5006	5889
Plant-pathogen interaction	2160	3035	2389	2704	2178	2776
Polycyclic aromatic hydrocarbon degradation	1700	1987	1666	1624	1401	1707
Polyketide sugar unit biosynthesis	2823	3335	3108	3237	2978	3213
Pores ion channels	5908	5502	5755	4879	4634	4938
Porphyrin and chlorophyll metabolism	10243	15579	11330	13436	10714	13981
PPAR signaling pathway	1776	2212	1873	1768	1497	1815
Prenyltransferases	4750	5679	4960	4919	4418	5073
Primary bile acid biosynthesis	510	531	501	513	496	505
Primary immunodeficiency	641	736	587	618	590	684
Prion diseases	134	170	132	164	138	176
Progesterone-mediated oocyte maturation	596	763	655	722	624	712
Propanoate metabolism	8795	10889	8789	8470	6971	8854
Prostate cancer	606	776	673	739	644	731
Proteasome	619	844	672	731	632	742
Protein digestion and absorption	279	213	307	246	292	247
Protein export	8948	11310	9556	9980	8755	10129
Protein folding and associated processing	9860	12166	10551	10692	9036	10816
Protein kinases	4380	6013	4347	5110	4041	5285
Protein processing in endoplasmic reticulum	1050	1278	1242	1194	1037	1132
Proximal tubule bicarbonate reclamation	502	544	472	457	401	434
Purine metabolism	31519	40703	33489	35868	30854	36435
Pyrimidine metabolism	26350	34343	28375	30977	26801	31435
Pyruvate metabolism	15413	20419	16111	17499	14678	17628
Renal cell carcinoma	171	162	142	72	74	102
Renin-angiotensin system	12	4	10	0	0	2
Replication, recombination and repair proteins	9988	13673	10417	11734	9576	12066
Restriction enzyme	2422	2943	2483	2657	2279	2606
Retinol metabolism	520	631	457	414	307	403
Riboflavin metabolism	3305	3580	3214	3010	2740	3117
Ribosome	34498	44546	37143	39638	34345	40164
Ribosome Biogenesis	20173	25955	21060	22666	19470	23162
Ribosome biogenesis in eukaryotes	739	940	760	778	670	810
RIG-I-like receptor signaling pathway	34	56	31	35	38	37
RNA degradation	6928	8421	7350	7528	6660	7693
RNA polymerase	2339	3117	2538	2714	2314	2744
RNA transport	1802	2520	1846	2254	1884	2310
Secondary bile acid biosynthesis	456	501	469	505	490	494
Secretion system	19221	23926	19652	19870	15567	20114
Selenocompound metabolism	5079	6717	5498	5858	4979	5990
Shigellosis	1	0	0	0	1	0
Signal transduction mechanisms	5987	8314	6207	7024	5697	7236
Small cell lung cancer	27	8	29	1	9	5
Sphingolipid metabolism	3286	3679	3474	3979	3802	4094
Sporulation	6196	13543	7062	12123	8909	11994
Staphylococcus aureus infection	193	243	143	125	117	166
Starch and sucrose metabolism	12029	17002	12715	15516	13314	15690
Steroid biosynthesis	47	22	26	5	3	5
Steroid hormone biosynthesis	211	166	200	147	156	165
Stilbenoid, diarylheptanoid and gingerol biosynthesis	53	57	48	22	21	39
Streptomycin biosynthesis	4588	5662	4993	5300	4769	5257
Styrene degradation	464	533	467	364	249	381
Sulfur metabolism	3694	4431	3644	3676	3258	3875
Sulfur relay system	3486	4499	3344	3395	2779	3551
Synthesis and degradation of ketone bodies	936	952	810	563	426	725
Systemic lupus erythematosus	17	11	12	1	1	0
Taurine and hypotaurine metabolism	1858	2180	1993	1944	1713	1982
Terpenoid backbone biosynthesis	8326	10564	8754	9161	7935	9436
Tetracycline biosynthesis	1838	2706	1990	2184	1744	2121
Thiamine metabolism	6824	8805	7209	7862	6779	8037
Toluene degradation	2115	1958	2268	1643	1509	1680
Toxoplasmosis	27	8	29	1	9	5
Transcription factors	20729	31982	21982	27600	21252	27991
Transcription machinery	13345	18245	14600	16344	13772	16652
Transcription related proteins	85	97	71	62	52	63
Translation factors	7905	10042	8565	9082	7930	9208
Translation proteins	13060	17045	13680	15047	12789	15249
Transporters	85003	130028	92222	114209	88963	117945
Tropane, piperidine and pyridine alkaloid biosynthesis	1642	2030	1736	1751	1470	1749
Tryptophan metabolism	4061	3964	3669	2742	2403	3104
Tuberculosis	2130	2731	2266	2375	2050	2409
Two-component system	22341	29987	23860	25480	19757	25919
Type I diabetes mellitus	938	1091	1018	1013	922	1013
Type II diabetes mellitus	643	916	691	787	672	775
Tyrosine metabolism	5069	6468	5038	5214	4284	5248
Ubiquinone and other terpenoid-quinone biosynthesis	3637	3289	3559	2836	2822	2941
Ubiquitin system	111	138	64	48	47	69
Valine, leucine and isoleucine biosynthesis	10637	14141	11384	12576	10619	12689
Valine, leucine and isoleucine degradation	5658	5684	5175	3899	3410	4454
Various types of N-glycan biosynthesis	17	28	32	17	8	12
Vibrio cholerae infection	3	2	1	0	2	0
Vibrio cholerae pathogenic cycle	996	1267	1030	1108	893	1110
Viral myocarditis	27	8	29	1	9	5
Vitamin B6 metabolism	3008	3761	3153	3395	2938	3379
Xylene degradation	580	1111	629	940	662	863
Zeatin biosynthesis	683	879	738	780	685	796
