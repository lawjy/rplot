#!/usr/bin/Rscript
# -*- coding: utf-8 -*-

suppressPackageStartupMessages(library(argparse))
docstring = "Description::\\n This Script is designes to plot Accumlation curve. \\n Author: Rojyang."
myparser <- ArgumentParser(description=docstring, formatter_class="argparse.RawTextHelpFormatter")
myparser$add_argument("rare_file", nargs=1, help="")
myparser$add_argument("-f","--prefix", nargs=1,  default="defalt", help=" Prefix of the figures")
myparser$add_argument('-o',"--outdir", default=getwd(), help="outdir")
args <- myparser$parse_args()

inputfile = args$rare_file
svgprefix = args$prefix
output = args$outdir
svgfile = paste(output, (paste(svgprefix, "svg", sep = ".")), sep = "/")

if(FALSE){
setwd("D:/LJY_Rscript")
inputfile = c("data/total.species.annotation.count")
svgfile = c("svg/accum.svg")
} 

library(vegan)
library(RColorBrewer)
library(ggplot2)
library(stringr)


data = read.table(inputfile, header = T, row.names = 1, sep = "\t", check.names = F)
dataT = t(data)
accum = specaccum(dataT,  method="random", permutations=200)
accumDF=data.frame(summary(accum), check.names = FALSE, stringsAsFactors = FALSE)
accumDF = subset(accumDF, select = -Var1)
paired=brewer.pal(n = 12, name = "Paired")


freq=c()
sample=c()
for(i in 1: length(rownames(accumDF))){
  a=as.integer(str_extract_all(as.character(accumDF$Freq[i]), "[0-9]+[0-9]"))
  b=as.numeric(str_extract(as.character(accumDF$Var2[i]), "\\d+"))
  freq=c(freq,a)
  sample=c(sample,b)
}
dataDF = data.frame(Samples=sample, Num=freq)

svg(svgfile, width = 10, height = 8)
ggplot(dataDF, aes(Samples, Num, group = Samples)) + 
  stat_boxplot(geom ='errorbar', width=0.2, position=position_dodge(1.05))+
  geom_boxplot(fill = paired[1]) +
  #theme_calc() +
  xlab("Samples") +
  ylab("Species") +
  labs(title="Accumulation Curves") +
  theme_bw()+
  theme(panel.grid=element_blank(), plot.title = element_text(lineheight=2.5, face="bold",hjust=0.5))

dev.off()

