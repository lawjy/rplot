#!/usr/bin/Rscript
# -*- coding: utf-8 -*-
suppressPackageStartupMessages(library(argparse))
docstring = "Description::\\n This Script is designes to plot Dilution curve. \\n Author: Rojyang."
myparser <- ArgumentParser(description=docstring, formatter_class="argparse.RawTextHelpFormatter")
myparser$add_argument("rare_file", nargs=1, help="")
myparser$add_argument("-f","--prefix", nargs=1,  default="defalt", help=" Prefix of the figures")
myparser$add_argument('-o',"--outdir", default=getwd(), help="outdir")
args <- myparser$parse_args()

library("reshape2")
library("ggplot2")

inputfile = args$rare_file
svgprefix = args$prefix
output = args$outdir
svgfile = paste(output, (paste(svgprefix, "svg", sep = ".")), sep = "/")
ylabs = svgprefix

if(FALSE){
setwd("D:/LJY_Rscript")
inputfile = c("data/observed_species.txt")
svgfile = c("svg/16srare.svg")
ylabs = c("obsever")
}

## 数据处理
data = read.table(inputfile, header = T, row.names = 1, sep = "\t")
dataS = subset(data, select = -iteration)
dataM = melt(dataS, id.var = c(colnames(dataS)[1]),variable.name = "sample", value.name = "species")  ##其他变量（measure.vars)均在variable列，value对应该variable的值
names(dataM)[1] = c("depth")


## R 绘制图
svg(svgfile, width = 10, height = 7)
ggplot(dataM, aes(depth, species, color = sample)) +
  geom_smooth(se=F, method = "lm",formula = y ~ log(x)) +  ## 添加回归公式
  theme_bw() +
  theme(panel.grid=element_blank(), plot.title = element_text(lineheight=2.5, face="bold",hjust=0.5)) +
  xlab("Number of Reads Sampled") + ylab(ylabs) +
  labs(title="Rarefaction Curves") 

dev.off()



##guides(fill = guide_legend(keywidth = 0.7, keyheight = 0.7)) + 
##theme(legend.position = "right", legend.text=element_text(size=7))
