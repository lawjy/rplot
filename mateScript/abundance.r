#!/usr/bin/Rscript
# -*- coding: utf-8 -*-

suppressPackageStartupMessages(library(argparse))
docstring = "Description::\\n This Script is designes to plot Dilution curve. \\n Author: Rojyang."
myparser <- ArgumentParser(description=docstring, formatter_class="argparse.RawTextHelpFormatter")
myparser$add_argument("r.abundance_file", nargs=1, help="")
myparser$add_argument("-f","--prefix", nargs=1,  default="defalt", help=" Prefix of the figures")
myparser$add_argument('-o',"--outdir", default=getwd(), help="outdir")
myparser$add_argument('-t',"--topN", default="30", help="top number ,default 30")
args <- myparser$parse_args()

inputfile = args$r.abundance_file
svgprefix = args$prefix
output = args$outdir
svgfile = paste(output, (paste(svgprefix, "svg", sep = ".")), sep = "/")
topN = as.numeric(args$topN)


if(FALSE){
setwd("D:/LJY_Rscript")
inputfile = c("data/total.S.annotation.relative_abundance")
svgfile = c("svg/top50.S.abundance.svg")
topN = as.numeric(30)
}


library(ggplot2)
library(RColorBrewer)
library(reshape2)
library(vegan)

paired=brewer.pal(n = 12, name = "Paired")
mycol=c("#00AED7","#FD9347","#C1E168","#319F8C","#FD6260","#FAD390",paired,colors()[c(47,381,59,70,27,33,65,428,611,634,656,178,515,518,26,490,104,112,222,13,258,294,83,121,562,451,97,367,100,300,400,275,200)])

data = read.table(inputfile, header = T, row.names = 1, sep = "\t", check.names = F)
dataTop = data[order(apply(data, 1, sum), decreasing = 1)[1:topN],]   ### 计算每个物种在所有样本的和，并进行排序取前N个物种
otherTmp = 1- colSums(dataTop)
other = data.frame(other = otherTmp)
dataN = rbind(dataTop, t(other))
dataT = as.data.frame(t(dataN))
dataT$sample = as.character(row.names(dataT))
dataTM = melt(dataT, id = "sample")


svg(svgfile, width = 10, height = 8)
ggplot(dataTM, aes(sample, value, fill=variable)) +
  geom_bar(stat="identity",position="stack") + 
  guides(fill = guide_legend(keywidth = 0.8, keyheight = 0.8, title="", size=6,ncol = 1 )) +
  scale_fill_manual(values = mycol) +
  theme(axis.text.x = element_text(size = 12,angle = 45,hjust = 1))
dev.off()
