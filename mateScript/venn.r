#!/usr/bin/Rscript
# -*- coding: utf-8 -*-

suppressPackageStartupMessages(library(argparse))
docstring = "Description::\\n This Script is designes to plot venn. \\n Author: Rojyang."
myparser <- ArgumentParser(description=docstring, formatter_class="argparse.RawTextHelpFormatter")
myparser$add_argument("count_file", nargs=1, help="")
myparser$add_argument("group_file", nargs=1, help="")
myparser$add_argument("-f","--prefix", nargs=1,  default="defalt", help=" Prefix of the figures")
myparser$add_argument('-o',"--outdir", default=getwd(), help="outdir")
args <- myparser$parse_args()

inputfile = args$count_file
groupfile = args$group_file
svgprefix = args$prefix
output = args$outdir
svgfile = paste(output, (paste(svgprefix, "svg", sep = ".")), sep = "/")



if(FALSE){
setwd("D:/LJY_Rscript")
inputfile = c("data/total.species.annotation.count")
groupfile = c("data/treatment.txt")
svgfile = c("svg/venn.svg")
}

library(ggplot2)
library(vegan)
library(VennDiagram)

data = read.table(inputfile, header = T, row.names = 1, sep = "\t", check.names = F)
group = read.table(groupfile, header = T, sep = "\t")
mycolor = c('#00AED7', '#FD9347', '#C1E168', '#319F8C',"#FF4040", "#228B22", "#FFFF33", "#0000FF", "#984EA3", "#FF7F00", "#A65628", "#F781BF", "#999999", "#458B74", "#A52A2A", "#8470FF", "#53868B", "#8B4513", "#6495ED", "#8B6508", "#556B2F", "#CD5B45", "#483D8B", "#EEC591", "#8B0A50", "#696969", "#8B6914", "#008B00", "#8B3A62", "#20B2AA", "#8B636C", "#473C8B", "#36648B", "#9ACD32")


dataN = data[as.character(group$Sample)]
dataT = t(dataN)
set.seed(1000)
dataR = rrarefy(dataT, min(rowSums(dataT)))
dataRT = as.data.frame(t(dataR))
names(dataRT) = c(as.character(group$treatment))

### 用来统计多个样品中共有或独有的 OTU 数目 对同个分组的多个样本统计平均值
tmp = apply(dataRT, 1, function(x) tapply(x, colnames(dataRT), mean))
dataT = t(tmp)


### 构建venn 图的输入文件
vennlist = list()
for (i in 1:ncol(dataT)) {
  tmpcolname = row.names(dataT[dataT[,i]>0,])
  vennlist[[colnames(dataT)[i]]] = tmpcolname
  
}


###plot 
venn.diagram(vennlist, filename = svgfile, height=5, width=5,  resolution = 300, fill = mycolor[1:ncol(dataT)], main="Venn 图" ,alpha = 0.80, cat.col = mycolor[1:ncol(dataT)],ontfamily = "serif",fontface = "plain",cex = 1.5, cat.cex = 1.2, cat.default.pos = "outer", cat.dist = 0.05, margin = 0.1,  lty = "blank",, lwd = 3, imagetype = "svg")

