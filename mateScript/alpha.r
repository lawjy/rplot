#!/usr/bin/Rscript
# -*- coding: utf-8 -*-

suppressPackageStartupMessages(library(argparse))
docstring = "Description::\\n This Script is designes to plot Alpha dicersity. \\n Author: Rojyang."
myparser <- ArgumentParser(description=docstring, formatter_class="argparse.RawTextHelpFormatter")
myparser$add_argument("count_file", nargs=1, help="")
myparser$add_argument("group_file", nargs=1, help = "")
myparser$add_argument("-f","--prefix", nargs=1,  default="defalt", help=" Prefix of the figures")
myparser$add_argument('-o',"--outdir", default=getwd(), help="outdir")
args <- myparser$parse_args()

inputfile = args$count_file
groupfile = args$group_file
svgprefix = args$prefix
output = args$outdir
svgfile = paste(output, (paste(svgprefix, "svg", sep = ".")), sep = "/")

if(FALSE){
  setwd("D:/LJY_Rscript")
  inputfile = c("data/total.species.annotation.count")
  groupfile = c("data/treatment.txt")
  svgfile = c("svg/alphabox.svg")
}

library(vegan)
library(RColorBrewer)
library(ggplot2)
library(reshape2)
library(ggthemes)

data = read.table(inputfile, header = T, row.names = 1, sep = "\t", check.names = "F")
##data = read.table(inputfile, header = T, row.names = 1, sep = "\t", check.names = F, comment.char = "", skip = 1)
group = read.table(groupfile, header = T, sep = "\t", check.names = "F")
mycol = brewer.pal(12, "Set3")
#display.brewer.all()

dataN = data[as.character(group$Sample)] 
dataT = t(dataN)
set.seed(1000)
dataTR = rrarefy(dataT, min(rowSums(dataT)))    ###基于最小值进行重抽样 类似RPKM

##Shannon
Shannon.wiener = diversity(dataTR, "shannon")
shannon = data.frame(values = Shannon.wiener)

#计算Simpson指数
simpson=data.frame(values=diversity(dataTR,"simpson"))


#计算Inverse Simpson指数
insimpson=data.frame(values=diversity(dataTR,"inv"))

#计算物种累计数
s=specnumber(dataTR)
sn=data.frame(values=s)

#计算Pielou均匀度指数
pielou=data.frame(values=Shannon.wiener/log(s))


#计算chao1, ACE
ca=data.frame(estimateR(dataTR)) #chao1, ACE
cat=data.frame(t(ca))

dataME = data.frame(Observed = sn$values, Chao1 = cat$S.chao1, Shannon = shannon$values, Peilou = pielou$values,Simpson=simpson$values,Insimpson=insimpson$values, g = group$treatment)
dataTM =melt(dataME, id = "g")

### R 绘制图
svg(svgfile, width = 10, height = 8)
ggplot(dataTM, aes(variable, value)) +
  stat_boxplot(aes(fill=g),geom ='errorbar', width=0.3, position=position_dodge(1.05)) +
  geom_boxplot(aes(fill = g), position=position_dodge(1.05)) +
  facet_wrap(~variable, scale = "free", nrow = 2 ) +
  scale_fill_manual(values=mycol) + 
  theme_calc() +
  labs(fill="Group") +
  xlab("") + ylab("")
dev.off()
    








